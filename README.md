# Mobile Qatar Payment
Mobile payment Training 

Manual reading
 Deployment in test portal
 Induction session
Steps:

Configured VPN as provided by Mobile team
Manual link was provided: https://manuals.pssupport.io/#all-updates
Tasks are being done
 

System Workflows in Mpay
Types of transactions
1. Banked to Banked (P2P)
2. Banked to Unbanked (P2P)
3. Unbanked to Unbanked (P2P)

Process in Transaction
1. Cash in Process
If we are depositing money in our wallet and asking PSP agent to deposit it in our wallet. This process is called Cash in. This will be debit from user's account and credit to PSP account

2. Cash out process
If we are withdrawing money from our wallet to cash or to purchase then the process is cash out.
In this case, user account will be credited and PSP account will be debited.

Step 1. Create Schemes as required

Types of Scheme:
1. Charges Schemes
These are the charges that the PSP might imply for cash in and cash out.

2. Limits Schemes
These are the number of times money can be withdrawn from wallet and the amount of money that can be withdrawn on daily, monthly or yearly basis.

3. Request Types Schemes

4. Commission Schemes
These are the schemes which help merchant to withdraw certain charge as commission for the service that has been provided.

5. Tax Schemes
These schemes collect tax that has been imposed by the governement as per the services provided

Step 2. Creating profile and adding schemes to profile

Create a Profile
Add the Schemes to the profile that have been created in step 1.

Step 3. Create Customers

Customers can be added by going to Customers Management and mobile number must be added
2. Mobile number must be of format 00974(and any eight digits) Ex. 0097487654321
3. Profile created in Step 2 can be added to the user as per the requirement
 

Step 4. Transactions view
1. Transactions like debit, credit, fees, tax etc can be seen in this section
2. This can be seen on Financial Queries
3. Transactions, Accounts, Journal Vouchers can help with the different view related to Accounts
4. It is always easy to search particular account from Mobile Number as it is always unique

Step 5. MPay Messages
1. This can be found in Messages inside Quiries
2. This view provides all the information related to transaction in json format.
3. Json format can be of this view:
Example:
{
"operation":"cashout",
"sender":"0097487569876",
"senderType":"M",
"lang":1,
"msgId":"c59f867b-7960-4367-80e4-d1ed176e03d0",
"tenant":"KHAL",
"extraData":[
{ "key":"amount", "value":"600" }
,
{ "key":"charges", "value":"3" }
,
{ "key":"receiver", "value":"KHALCO" }
,
{ "key":"receiverType", "value":"C" }
,
{ "key":"senderAccount", "value":"KHAL001" }
,
{ "key":"notes" }
,

{ "key":"source", "value":"MPAY" }
]
}

Step 6. Notifications
1. This can be found inside Notifications of Quiries section
2. This consistes of all the notifications from SMS,E-mail and so on
3. Also this view show the status of sucess and rejection of transaction

 

 

 

Tasks:

Day 1 Task:

Create two profile: Profile A and Profile B
Profile A has limit for Cash out 1000 QR Daily
Profile B has limit for Cash out with 500 QR Daily
Profile A has charges of 2 QR with limit up to 1000 QR
Profile B has charges of 2 QR with limit up to 500 QR
Create customer C1 and assign Profile A to it.
Create Customer C2 and assign Profile B to it.
Cash in C1 with 1500 QR.
Cash in C2 with 1000 QR
Cash out C1 with 700 QR
Cash out C2 with 400 QR
Cash out C1 with 800 QR
Cash out C2 with 100 QR
Configure Person to Merchant transaction to be Direct Credit with minimum amount of 0 and maximum amount of 1000 QR.
Day 2 Task:

Create Charge Scheme for Cash Out (Slice up to 100 with 1 QR Charge fixed)
Create Charge Scheme for Cash Out (Slice up to 500 with 2 QR Charge fixed)
Create Charge Scheme for Cash Out (Slice up to 700 with 3 QR Charge Percentage)
Cash out with 90 QR
Cash out with 150 QR
Cash out with 600 QR
 

You will have to check the transaction view and understand the Generated Journal Voucher.

You will have to check the balances of the customer and the impact over it when you do Cash in/Cash out.

You will have to read the Cash out/in message request from Messages view under quries.

 

Day 3 Task:

A new PSP has launched in the Market. The PSP decided to work with unbanked customers and corporates with the following Client types information:

1. customer can make up to 3 wallets under the same National ID

2. Merchant can register up to 100 wallets under the same National ID

3. Agent can register up to 500 wallets under the same national ID.

PSP created 3 profiles as the following:

1. Refugees Profile which has no fees on Cash In or Cash out, This profile is not able to do P2P transactions. (Wallet CAP 500 QR).

Daily Limits: P2P: Count = 5 and amount = 200)

Daily Limits: Cash Out: Count = 2 and amount = 500)

2. Regular Profile which has fees on on Cash out with 2 QR for transaction amounts up to 100 QR, and 5 QR up 500 QR, and 3 QR for transaction amount up to 1000 (100% sender , 25% receiver). (Wallet Cap Unlimited)

Daily Limits: P2P: Count = 10 and amount = 800)

Daily Limits: Cash Out: Count = 2 and amount = 100)

3. Children profile which has no fees, this profile will not be able to do any transaction but the Merchant Payment (POS Transaction) and Customer Cash in. (Wallet Cap 50 QR).

Daily Limits: POS: Count = 2 and amount = 50)

1. United nations decided to open 2 Wallets for refugees and to do Cash in for them with 100 QR.

2. One of the united nations customers decided to do Cash out with 50 QR.

3. Someone decided to open a wallets for him and his daughter under the same national id.

4. The father decided to do cash in for his wallet with 1000 QR and for his daughter with 30 QR.

5. The father decided to do cash out for his wallet with 200 QR.

6. The father decided to do cash in for his daughter with 50 QR.

What is needed is to configure the following:

1. POS transaction (Sender customer, Receiver Merchant) Direct Credit.

2. Create Merchant for the daughter's school and assign the Regular Profile to it.

3. Show the transaction which has been made with the status of the transaction (Including Original amount, fees and status).

4. Show the customer balances and calculate how much e-Money the PSP has in the market.

5. Create Multiple Corporate Services for the merchant created in point number 2.

6. Check the Cash out JSON message and try to figure out what is the POS transaction JSON.


